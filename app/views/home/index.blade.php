<!doctype html>
<html lang="pt-br">
<head>
	<!-- Meta Tags -->
	<meta charset="UTF-8">
	<title>Tweet Map</title>
	<!-- CSS -->
	{{ HTML::style('http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,700,700italic,900') }}
	{{ HTML::style('css/main.css') }}
</head>
<body>

	<!-- Sidebar section left -->
	<aside class="side-content user-content">
		<div class="user-id cf">
			<img src="https://pbs.twimg.com/profile_images/1660794706/eightbit-8e79dab8-c7cd-4edc-b616-6e1ea2414e00_bigger.png" width="46" height="46" alt="" class="user-avatar">
			<hgroup class="user-names">
				<h2 class="user-name">Cezar Luiz</h2>
				<h2 class="user-screen-name">@cezar_luiz</h2>
			</hgroup>
		</div>


		<ul class="user-follows cf">
			<li>184 <br> <span>Seguindo</span></li>
			<li>184 <br> <span>Seguidores</span></li>
		</ul>

		<ul class="user-info">
			<li>Curitiba, PR</li>
			<li>Director, Platform at Twitter. Detroit and Boston export. Foodie and over-the-hill hockey player. @devon's lesser half</li>
		</ul>

		<!-- About project -->
		<a href="/sobre" class="btn btn-ajax">Sobre o projeto</a>
	</aside>


	<!-- The map -->
	<section class="the-map" id="the-map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3602.6713823197447!2d-49.28784774999998!3d-25.449247800000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce3865e30cd63%3A0x3e097326338a8a7c!2sAgencia+IMAM+Publicidade+e+Propaganda.!5e0!3m2!1spt-BR!2sbr!4v1392861359751" width="100%" height="100%" frameborder="0" style="border:0"></iframe>
	</section>

	<!-- Tweets -->
	<aside class="side-content tweets-content">
		<h3 class="section-title">Seus tweets recents</h3>

		<section class="tweets-section">
			<a href="#" class="tweet">
				<p class="tweet-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, ducimus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				<span class="tweet-time">19 09 2013 por <strong>Cezar Luiz</strong> em <strong>Barba Café</strong></span>
			</a>
			<a href="#" class="tweet">
				<p class="tweet-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, ducimus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				<span class="tweet-time">19 09 2013</span>
			</a>
			<a href="#" class="tweet">
				<p class="tweet-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, ducimus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				<span class="tweet-time">19 09 2013</span>
			</a>
			<a href="#" class="tweet">
				<p class="tweet-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, ducimus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				<span class="tweet-time">19 09 2013</span>
			</a>
			<a href="#" class="tweet">
				<p class="tweet-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, ducimus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				<span class="tweet-time">19 09 2013</span>
			</a>
			<a href="#" class="tweet">
				<p class="tweet-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, ducimus. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				<span class="tweet-time">19 09 2013</span>
			</a>
		</section>

		<a href="#" class="btn">Carregar Mais</a>
	</aside>

	{{ HTML::script('js/modal.js') }}
</body>
</html>
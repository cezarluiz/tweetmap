/**
 * Modal
 */
window.Modal = (function(window, document) {
	"use strict";

	/**
	 * The modal elements
	 */
	var app = {
		modal: document.createElement('div'),
		close: document.createElement('a'),
		content: document.createElement('div')
	}

	/**
	 * Options
	 */
	var opt = {
		modalClass: 'modal',
		closeBtnClass: 'modal-close',
		contentClass: 'modal-content',
		closedClass: 'closed',
		openClass: 'opened',
		ajaxType: 'GET',
		ajaxUrl: '',
		parent: document.body,
		timeout: 5000,
		timeClass: 300,
		event: 'click',
		target: ''
		data: {},
		onSuccess: function(r) {
			app.content.innerHTML = r.responseText;
		},
		onError: function(e) {

		},
		onProgress: function (e) {

		}
	}


	/**
	 * Methods
	 */
	function makeModal() {
		/* Set elements options */
		app.modal.classList.add(opt.modalClass);
		app.modal.appendChild(app.close);
		app.modal.appendChild(app.content);

		/* Close buton */
		app.close.classList.add(opt.closeBtnClass);
		app.close.href = '#';
		app.close.innerHTML = 'X';

		/* Content */
		app.content.classList.add('modal-content');

		/* Append on parent */
		opt.parent.appendChild(app.modal);

		/**
		 * Show with delay
		 */
		window.setTimeout(function() {
			app.modal.classList.add(opt.openClass);
		}, 0);
	}

	/**
	 * Prototypes
	 */
	Object.prototype.extend = function(x) {
       for(var i in x) {
          this[i] = x[i];
       }
    };

    /**
     * Create AJAX object
     */
	var xhr = new XMLHttpRequest;

	function ajaxSuccess (e) {
		if (xhr.status >= 200 && xhr.status < 400 && xhr.readyState === 4) {
			opt.onSuccess(xhr);

			/**
			 * Make
			 */
			makeModal();
		} else {
			opt.onError(e);
		}
	}

	function updateProgress (e) {
		opt.onProgress(e);
	}

	function ajaxError (e) {
		opt.onError(e);
	}

	/**
	 * Close
	 */
	app.close.addEventListener('click', function (e) {
		e.preventDefault();

		app.modal.classList.add(opt.closedClass);

		/* Remove the node */
		window.setTimeout(function () {
			app.modal.parentNode.removeChild(app.modal);
			app.modal.classList.remove(opt.closedClass, opt.openClass);
		}, opt.timeClass);
	});


	/**
	 * Public scope
	 */
	return function(o) {
		/**
		 * Extend options
		 */
		opt.extend(o);

		/**
		 * Initializes a request
		 */
		xhr.open(opt.ajaxType, opt.ajaxUrl, true);

		xhr.timeout = opt.timeout;

		xhr.addEventListener('load', ajaxSuccess);
		xhr.addEventListener('progress', updateProgress, false);
		xhr.addEventListener('error', ajaxError, false);
		xhr.addEventListener('abort', ajaxError, false);
		xhr.addEventListener('timeout', ajaxError, false);

		xhr.send(JSON.stringify(opt.data));
	};

}(window, document));

/**
 * Modal sobre o projeto
 */
var btnAjax = document.querySelectorAll('.btn-ajax');

for (var i = 0, t = btnAjax.length; i < t; i++) {
	var el = btnAjax[i];

	el.addEventListener('click', function (e) {
		Modal({
			ajaxUrl: this.href
		});

		e.preventDefault();
	});
}